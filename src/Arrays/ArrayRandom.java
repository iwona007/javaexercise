package Arrays;

import java.util.Random;

public class ArrayRandom {
    public static void main(String[] args) {

        //zupełnij tablice jednowymiarową losowymi liczbami od 1 do 10.
        //Znajdź indeks i wartość najmniejszej liczby w tablicy.
        Random losowanie = new Random();

        int[] tablica = new int[9];
        int losoweLiczby = 26;
        for (int i = 0; i < tablica.length; ++i) {
            tablica[i] += losoweLiczby;
            System.out.print(losowanie.nextInt(26)+ "\t");
        }
        System.out.println();


        ///drugi sposób

        int[] tablica2 = new int[10];
        int min = 1;
        int max = 10;
        int range = max - min + 1;
        //int licznikIndeks = 0;
        for (int j = 0; j < tablica2.length; j++) {
            tablica2[j] = (int) (Math.random() * range) + min;
            System.out.print(tablica2[j] + "\t");



        }
    }
}
