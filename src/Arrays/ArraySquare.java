package Arrays;

public class ArraySquare {
    public static void main(String[] args) {
        // 1.Przy pomocy pętli zmień zawartość każdego elementu tablicy naturalne na jego kwadrat
        //tablica:{1,4,9,16,...,100}

        double[] tablicaNaturalne = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for ( int indeks = 0; indeks < tablicaNaturalne.length; indeks++) {
            tablicaNaturalne[indeks] = Math.pow((indeks +1), 2);
            System.out.println("* " + tablicaNaturalne[indeks] );
        }
    } // 27 march 2019
}
