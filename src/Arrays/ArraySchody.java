package Arrays;

public class ArraySchody {
    public static void main(String[] args) {

        // Stwórz tablicę dwuwymiarową w formie schodów przy użyciu pętli
//1
//2 3
//4 5 6
//7 8 9 10
        int[][] tablicaSchody = new int [4][];
        int licznikLiczbNaturalnych =0;
       tablicaSchody [0] = new int [1];
        tablicaSchody [1] = new int [2];
        tablicaSchody [2] = new int [3];
        tablicaSchody [3] = new int [4];
        //albo
       // int [][] tablicaSchody = {{1}, {2, 3}, {4, 5, 6}, {7, 8, 9, 10}};

        for (int licznikWiersza = 0; licznikWiersza<tablicaSchody.length; licznikWiersza++){
            for(int licznikKolumn = 0; licznikKolumn< tablicaSchody[licznikWiersza].length; licznikKolumn++){
            tablicaSchody[licznikWiersza][licznikKolumn] = ++ licznikLiczbNaturalnych;
            /*Nie możemy brac tej metody gdy nie znamy jaki jest drogi wymiar, bo nie wiemy przez jaki
            wymiar kolumn pomnożyć.
            tablicaSchody[licznikWiersza][licznikKolumn] = licznikWiersza * 2 +licznikKolumn +1; */

                System.out.print(tablicaSchody[licznikWiersza][licznikKolumn] +"\t");
            }
            System.out.println();
        }
    }
}
