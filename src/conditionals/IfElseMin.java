package Conditionals;

import java.util.Scanner;

public class IfElseMin {
    public static void main(String[] args) {
        //Wczytać 3 różne liczby od użytkownika, wypisać, która z nich jest najmniejsza

        System.out.println("Podaj Liczbę 1: ");
        int podanaLiczba1 = new Scanner(System.in).nextInt();
        System.out.println("Podaj Liczbę 2: ");
        int podanaLiczba2 = new Scanner(System.in).nextInt();
        System.out.println("Podaj Liczbę 3: ");
        int podanaLiczba3 = new Scanner(System.in).nextInt();

        if (podanaLiczba1 < podanaLiczba2 && podanaLiczba1 < podanaLiczba3) {
            System.out.println("Liczba 1 jest najmniejsza.");
        } else if ((podanaLiczba2 < podanaLiczba3) && (podanaLiczba1 < podanaLiczba3)) {
            System.out.println("Liczba 2 jest najmniejsza.");
        } else if (podanaLiczba3 < podanaLiczba2 && podanaLiczba3 < podanaLiczba1) {
            System.out.println("Liczba 3 jest najmniejsza.");
        }
    }
}


// 20 march 2019

