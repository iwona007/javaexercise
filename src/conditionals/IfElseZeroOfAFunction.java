package Conditionals;

import java.util.Scanner;

public class IfElseZeroOfAFunction {
    public static void main(String[] args) {
        //Napisać program liczący miejsca zerowe funkcji kwadratowej.

        System.out.println("Podaj wartość zmiennej a:");
        double podanaZmiennaA = new Scanner(System.in).nextDouble();
        System.out.println("Podaj wartość zmiennej b:");
        double podannaZmiennaB = new Scanner(System.in).nextDouble();
        System.out.println("Podaj wartość zmiennej c:");
        double podannaZmiennaC = new Scanner(System.in).nextDouble();

        double podanaDelta = Math.pow(podannaZmiennaB , 2) - 4 * podanaZmiennaA  * podannaZmiennaC;

        if (podanaDelta == 0) {
            double miejsceZerowe = -podannaZmiennaB / 2 * podanaZmiennaA;
            System.out.println("Delta jest rwna 0 dlatego jest 1 miejsce zerowe które wynosi: " + miejsceZerowe);
        } else if (podanaDelta > 0) {
            double miejsceZerowe1 = (-podannaZmiennaB  + Math.sqrt(podanaDelta)) / (2 * podanaZmiennaA) ;
            double miejsceZerowe2 = (-podannaZmiennaB  - Math.sqrt(podanaDelta)) / (2 * podanaZmiennaA) ;
            System.out.println("Delta jest > 0 dlatego mamy dwa miejsca zerowe. Miejsce zerowe 1 wynosi: " + miejsceZerowe1
            + '\n' + "Miejsce zerowe 2 wynosi: " + miejsceZerowe2);
        } else if (podanaDelta < 0) {
            System.out.println("Delta jest < 0 dlatego nie ma miejsc zerowych.");
        }
// 20 March 2019
    }
}

