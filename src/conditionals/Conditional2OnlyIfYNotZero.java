package Conditionals;

import java.util.Scanner;

public class Conditional2OnlyIfYNotZero {
    public static void main(String[] asgr){

        /*Wczytaj od użytkownika liczbę x i y, policz ich iloraz (x/y) tylko gdy y nie jest zerem */

        System.out.println("Podaj zmienną x: ");
        double podanaZmiennaX = new Scanner(System.in).nextDouble();
        System.out.println("Podaj Zmienną y: ");
        double podanaZmiennaY = new Scanner(System.in).nextDouble();

        if(podanaZmiennaY != 0 ){
            double ilorazXDoY = podanaZmiennaX / podanaZmiennaY;
            System.out.println("Iloraz X do Y wynosci: " + ilorazXDoY);
        }
    }
}
