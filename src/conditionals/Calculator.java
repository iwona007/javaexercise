package Conditionals;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {

        /*Napisać program -kalkulator.Wczytuje od użytkownika 2 liczby, wczyta znak
        działania(1 = +, 2 = -, 3 = *, 4 = /)i zwróci wynik działania.
        Jeśli będzie to dzielenie to powinien sprawdzić, czy druga liczba nie jest zerem.*/

        System.out.println("Podaj zmienną A:");
        double podanaZmiennaA = new Scanner(System.in).nextDouble();
        System.out.println("Podaj zmienną B:");
        double podanaZmiennaB = new Scanner(System.in).nextDouble();

        System.out.println("Wybierz kod operacji: 1 = +, 2 = -, 3 = *, 4 = /)");
        int kodOperacji = new Scanner(System.in).nextInt();

        if (kodOperacji == 1) {
            double dodawanie = podanaZmiennaA + podanaZmiennaB;
            System.out.println("Wynik dodawanie równa się: " + dodawanie);
        } else if (kodOperacji == 2) {
            double odejmowanie = podanaZmiennaA - podanaZmiennaB;
            System.out.println("Wynik odejmowania równa się: " + odejmowanie);
        } else if (kodOperacji == 3) {
            double mnożenie = podanaZmiennaA * podanaZmiennaB;
            System.out.println("Wynik mnożenia równa się: " + mnożenie);
       /* } else if (kodOperacji == 4) {
            if (podannaZmiennaB != 0) {
                double dzielenie = podanaZmiennaA / podanaZmiennaB;
                System.out.println("wynik dzielenia: " + dzielenie);
            } else {
                System.out.println("Druga zmienna jest zerem. Podaj cyfrę różna od zera.");*/

                //lub w taki sposób

                  } else if (kodOperacji == 4 && podanaZmiennaB != 0) {

            double dzielenie = podanaZmiennaA / podanaZmiennaB;
            System.out.println("wynik dzielenia: " + dzielenie);
        } else {
            System.out.println("Operacja niedozwolana. Podaj druga cyfrę różną od 0. ");

            }
        }
    }
