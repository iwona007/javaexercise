package Conditionals;

import java.util.Scanner;

public class CalculatorCharMarks {
    public static void main(String[] args) {

        System.out.println("Podaj zmienna A:");
        double podanaZmiennaA = new Scanner(System.in).nextDouble();
        System.out.println(" Podaj zmienną B:");
        double podanaZmiennaB = new Scanner(System.in).nextDouble();

        System.out.println("POdaj kod operacji: +, -, *, /");
        char kodOperacji = new Scanner(System.in).nextLine().charAt(0);

        if (kodOperacji == 43) {
            double dodawanie = podanaZmiennaA + podanaZmiennaB;
            System.out.println("Wynik dodawanie równa się: " + dodawanie);
        }else if(kodOperacji == 45) {
            double odejmowanie = podanaZmiennaA - podanaZmiennaB;
            System.out.println("Wynik odejmowania równa się: " + odejmowanie);
        }else if (kodOperacji == 42) {
            double mnożenie = podanaZmiennaA * podanaZmiennaB;
            System.out.println("Wynik mnożenia równa się: " + mnożenie);
        }else if (kodOperacji == 47) {
            if (podanaZmiennaB != 0){
                double dzielenie = podanaZmiennaA / podanaZmiennaB;
                System.out.println("wynik dzielenia: " + dzielenie);
            }else{
                System.out.println("Druga zmienna jest zerem. Podaj cyfrę różna od zera.");
            }

        }
//21 March 2019
    }
}
