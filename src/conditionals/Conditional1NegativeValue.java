package Conditionals;

import java.util.Scanner;

public class Conditional1NegativeValue {
    public static void main (String[] args){

        /*Wczytaj od użytkownika liczbę i wypisz napis "Podałeś za małą wartość" tylko gdy podana liczba będzie ujemna
         */

        System.out.println("Podaj dowolną liczbę;");
        double podanaLiczba = new Scanner(System.in).nextDouble();

        if (podanaLiczba < 0 ){
            System.out.println("Podałeś za małą wartość");
        }
        System.out.println("Podałeś liczbę: " + podanaLiczba);



    }
}
