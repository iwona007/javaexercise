package Conditionals;

import java.util.Scanner;

public class ElseIfPitagoras {
    public static void main(String[] args) {

        //Napisać program wczytujący od użytkownika długości boków A i C oraz liczący z twierdzenia Pitagorasa długość boku B.

        System.out.println("Podaj długośc boku A: ");
        double podanaDlugoscBokuA = new Scanner(System.in).nextDouble();
        System.out.println("Podaj długośc boku C: ");
        double podanaDlugoscBokuC = new Scanner(System.in).nextDouble();

        if (podanaDlugoscBokuA == 0 || podanaDlugoscBokuC == 0  ){
            System.out.println("Podaj inną wartośc niż zero.");
        } else {
            double dlugoscBokuB = Math.sqrt(Math.pow(-podanaDlugoscBokuA, 2) + Math.pow(podanaDlugoscBokuC, 2));
            System.out.println("Długość boku B wynosi: " + dlugoscBokuB);
        }
//20 March 2019
    }
}
