package Strumienie.InStrumienie;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class StrumienZPliku {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("ala.txt");

        int readValue = fileInputStream.read();

        System.out.println((char)readValue);
        System.out.println((char) fileInputStream.read());
        fileInputStream.close();

        // wczytanie całego pliku
        System.out.println("**********************");
        FileInputStream fis = new FileInputStream("ala.txt");
        while(true){
            int read2 = fis.read();
            if(read2> 0 ){
                System.out.println((char) read2);
            } else{
                break;
            }
        }
        System.out.println("*************************************");
        int read3;
        while((read3 = fis.read())>0){
            System.out.println((char)read3);
        }

    }
}
