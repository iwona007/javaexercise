package Strumienie.InStrumienie;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CharMnozenie {
    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream = new FileInputStream("Mnozenie2.txt");

        int readValue = fileInputStream.read();
        System.out.println("Czyta jako Dec z tablicy ASCII: " + readValue);
        System.out.println("Przeczytany jako znak ASCII: " + (char)readValue);

        if(readValue >= 48 && readValue <= 57){
            System.out.println("Znaleziono cyfrę");

            // Zamiana kodu ASCII na wartość cyfry. 48 to zero

            int value = readValue - 48;
            System.out.println("Liczba pomnożona przez 5 to: " + value * 5);
        } else{
            System.out.println("Wczytany znak nie jest cyfrą");
        }
        fileInputStream.close();
    }
}
