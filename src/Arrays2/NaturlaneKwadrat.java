package Arrays2;

public class NaturlaneKwadrat {
    public static void main(String[] args) {

        //      Przy pomocy pętli zmień zawartość każdego elementu tablicy naturalne na jego kwadrat
// oczekiwany wynik: tablica:{1,4,9,16,...,100}

        int[] tablica = new int[10];
        tablica[0] = 1;
        tablica[1] = 2;
        tablica[2] = 3;
        tablica[3] = 4;
        tablica[4] = 5;
        tablica[5] = 6;
        tablica[6] = 7;
        tablica[7] = 8;
        tablica[8] = 9;
        tablica[9] = 10;

        System.out.print("Tablica:{");
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = (int) Math.pow((i + 1), 2);

            System.out.print(tablica[i] + (i == tablica.length - 1 ? "" : ", "));
        }

        System.out.print("}");


    }
}
