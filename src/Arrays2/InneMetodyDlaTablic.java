package Arrays2;

import java.util.Arrays;

public class InneMetodyDlaTablic {
    //wypełnianie tablicy podanym elementem
    public static void main(String[] args) {

        int[] tablicaPiatek = new int[7];
        Arrays.fill(tablicaPiatek, 5);
        System.out.println("Zawtrtość tabilcy: " + Arrays.toString(tablicaPiatek));

        //porównanie i sortowanie

        int[] oryginal2 = {78, 8, 9, 4, 7, 2, 3, 6, 5, 8, 9, 2, 3, 5, 8, 8};
        int[] parametry = {10, 15, 20, 36, 45, 78, 90, 1, 2, 3, 4, 5};
        boolean czyRowne = Arrays.equals(oryginal2, parametry);

        Arrays.sort(oryginal2);

        System.out.println("Czy tablice są sobie równe?: " + czyRowne);
        System.out.println("Posortowana tablica orginal2: " + Arrays.toString(oryginal2));

// szukanie

        System.out.println("szukany element w paramtry wyności: " + Arrays.binarySearch(parametry, 3));


   }
}


