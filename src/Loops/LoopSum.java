package Loops;

import java.util.Scanner;

public class LoopSum {
    public static void main(String[] args) {
        //   Zsumuj wszystkie liczby od 0 do liczby podanej przez użytkownika i wydrukuj wynik w konsoli.

        System.out.println("Podaj dowolną liczbę: ");
        int podanaLiczba = new Scanner(System.in).nextInt();

        int licznik = 0;
        int sum = 0;
        while (licznik <= podanaLiczba) {
            sum = sum + licznik;
            System.out.println(licznik);
            licznik++;
        }

        System.out.println("Suma od 0 do liczby podanej przez użytkownika: " + sum);
    }
}
