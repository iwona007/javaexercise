package Loops;

import java.util.Scanner;

public class LoopRectangleProstokoat {
    public static void main(String[] args) {
        //Użytkownik podaje dwie liczby (x i y), wydrukuj w konsoli z gwiazdek prostokąt o wymiarach x*y

        System.out.println("Podaj szerokość prostokatu Y: ");
        int podanaSzerokosciY = new Scanner(System.in).nextInt();
        System.out.println("Podaj długość prostokata X: ");
        int podanaDługośćX = new Scanner(System.in).nextInt();

        int licznikSzerokosci = 0;
        int licznikDlugosci = 0;

        while (licznikDlugosci < podanaDługośćX) {
            while (licznikSzerokosci < podanaSzerokosciY) {
                System.out.print("*");
                licznikSzerokosci++;
            }
            System.out.println();
            licznikSzerokosci = 0;
            licznikDlugosci++;
        }
    } // 22- March 2019

        /*int szer = 20;

        int wys = 10;


        for (int i = 0; i < wys; i++) {

            for (int j = 0; j < szer; j++)

                System.out.print('*');

            System.out.println();
        }*/
}
