package Loops;

import java.util.Scanner;

public class LoopFactorialSilnia {
    public static void main(String[] args) {
        //Policz silnię z X (silnia 5! = 1*2*3*4*5)
        System.out.println("Podaj liczbę Silinii X: ");
        int podanaSilniaX = new Scanner(System.in).nextInt();

        int licznik = 1;
        int silnia = 1;
        while (licznik <= podanaSilniaX) {
            silnia = silnia * licznik;
            System.out.println(licznik);
            licznik++;
        }

        System.out.println("Silani z X: " + silnia);
    }
} // 22 march 2019