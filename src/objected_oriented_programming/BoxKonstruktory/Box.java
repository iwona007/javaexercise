package objected_oriented_programming.BoxKonstruktory;

public class Box {
    int x;
    int y;
    int z;

    public Box() {
        x = 10;
        y = 10;
        z = 10;
    }

    public Box(int x) {
        this.x = x;

    }

    public Box(int x, int y) {
        this.x = x;
        this.y = y;

    }

    public Box(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    int volume() {
        int objentosc = x * y * z;
        return objentosc;

    }
}
