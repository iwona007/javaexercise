package objected_oriented_programming.VehicleDziedziczenie;

public class Mainvehicle {
    public static void main(String[] args) {

        Vehicle vehicle = new Vehicle();
        System.out.println("Rok produkcji " + vehicle.manufactureYear);

        Bike bike = new Bike();
        System.out.println("Pok produkcji: " + bike.manufactureYear);

        Car car = new Car();
        System.out.println("Rok produkcji " + car.manufactureYear);

        Car car1 = new Car();

        car1.manufactureYear = 2007;
        System.out.println("Rok produkcji car1: " + car1.manufactureYear);

        System.out.println(car);
        System.out.println(car.toString());
    }
}
