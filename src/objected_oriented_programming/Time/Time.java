package objected_oriented_programming.Time;


public class Time {
    private int hours;
    private int minutes;
    static final int MAX_HOURS = 24;
    static final int MAX_MINUTES = 60;

    public Time() {
    }

    public Time(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    void addHours(int hoursToAdd) {

        /*this.hours = this.hours + hoursToAdd;
        while (this.hours >= MAX_HOURS) {
            this.hours -= MAX_HOURS;
        }*/

        //albo
        this.hours = (this.hours + hoursToAdd) % MAX_HOURS;
    }

    public void addMinutes(int minutesToAdd) {
      /*  this.minutes = this.minutes + minutesToAdd;
        while (this.minutes >= MAX_MINUTES) {
            this.minutes -= MAX_MINUTES;
            addHours(1);
        }*/

        //albo
        int newMinutes = this.minutes + minutesToAdd;
        this.minutes = newMinutes % MAX_MINUTES;
        int hoursToAdd = newMinutes / MAX_MINUTES;
        addHours(hoursToAdd);

    }

    public double timeDiff(Time time4) {
        double roznica =  this.hours - time4.hours;
        return roznica;
    }


    @Override
    public String toString() {
        return "Jest godzina:minut " + hours + ":" + minutes;
    }

    @Override
    public boolean equals(Object objectToCompare) {
        if (this == objectToCompare) {
            return true;
        }
        if (objectToCompare == null) {
            return false;
        }
        if (getClass() != objectToCompare.getClass()) {
            return false;
        }
        Time timeToCompare = (Time) objectToCompare;

        if (hours != timeToCompare.hours) {
            return false;
        }
        if (minutes != timeToCompare.minutes) {
            return false;
        }
        return true;
    }

    void wypiszGodzine() {
        System.out.println("Jest godzina:minut " + hours + ":" + minutes);
    }

}
