package objected_oriented_programming.Time;

public class MainTime {
    public static void main(String[] args) {

        Time time = new Time(24, 30);
        Time time1 = new Time(15, 15);
        Time time2 = new Time(15, 15);
        Time time3 = new Time(10, 30);
        Time time4 = new Time(15, 50);

        time.addHours(5);
        time.addMinutes(20);

        double roznica = time4.timeDiff(time3);
        System.out.println("roznica: " + roznica);

        boolean areEquals = time1.equals(time2);
        System.out.println("Are equal: " + areEquals);

        System.out.println(time2);
        // System.out.println(time.toString());
        time.wypiszGodzine();


    }


}

