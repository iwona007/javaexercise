package objected_oriented_programming.Animals2DziedziczenieOverread;

public class Dog extends  Animal {

    @Override
    public void speak(){
        System.out.println("hou, hou");
    }

    @Override
    public String toString(){
        return "Jestem psem i mam lat: " + getAge();
    }
}
