package objected_oriented_programming.Animals2DziedziczenieOverread;

public class Cat extends Animal {

    @Override
    public void speak(){
        System.out.println("Miau, Miau.");
    }

    @Override
    public String toString(){
        return "Jestem kotem i mam lat: " + getAge();
    }
}
