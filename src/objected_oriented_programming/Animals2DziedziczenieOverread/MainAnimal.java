package objected_oriented_programming.Animals2DziedziczenieOverread;

public class MainAnimal {
    public static void main(String[] args) {

        Animal animal = new Animal();
        animal.setAge(3);

        Cat cat1 = new Cat();
        cat1.setAge(5);

        Dog dog1 = new Dog();
        dog1.setAge(2);

        Bird bird1 = new Bird();
        bird1.setAge(1);

        System.out.println(animal);
        System.out.println(cat1);
        System.out.println(dog1);
        System.out.println(bird1);
    }
}
