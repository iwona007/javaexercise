package objected_oriented_programming.Animals2DziedziczenieOverread;

public class Animal {

   private int age;


   public Animal(){

   }

   public Animal(int age){
       this.age = age;

   }
    public int getAge(){
        return age;
    }

    public void setAge(int age){
        this.age= age;
    }

    public void  speak(){
        System.out.println("Jestem zwierzeciem i mówię.");
    }

    @Override
    public String toString(){
       return "jestem zwierzenciem i mam lat " + age;
    }
}
