package objected_oriented_programming.Animals;

import java.util.ArrayList;
import java.util.List;

public class Animal {
    String name;
    String species;
    String podgatunek;
    int age;

    public Animal() {
        this.name = name;
        this.species = species;
        this.podgatunek = podgatunek;
        this.age = age;
    }


    void wyswietlDaneZwierzecia() {
        System.out.println("\n" + "********" + "\n" + "imię: " + name);
        System.out.println("gatunek " + species);
        System.out.println("podgatunek " + podgatunek);
        System.out.println("wiek: " + age);
    }

    void dajGlos() {
        boolean jakiPodgatunek = podgatunek.equalsIgnoreCase("Cat");
        if (jakiPodgatunek){
            System.out.println("Miauuu");
        }else if (podgatunek.equalsIgnoreCase("goat")){
            System.out.println("bee");
        }else if (podgatunek.equalsIgnoreCase("cow")){
            System.out.println("Muuu");
        }else {
            System.out.println("Nie ma tego zwierzećia w bazie");
        }
    }


}

