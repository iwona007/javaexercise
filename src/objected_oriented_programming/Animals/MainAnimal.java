package objected_oriented_programming.Animals;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainAnimal {
    public static void main(String[] args) {

        Animal cat = new Animal();
        cat.name = "Garfild";
        cat.species = "Ssak";
        cat.podgatunek = "cat";
        cat.age = 1;

        Dog pies = new Dog();

        pies.szczekaj();
        pies.warcz();
        pies.wypiszWiek();
        System.out.println(pies.zmienWiek());

        Animal goat = new Animal();
        System.out.println("Podaj imię kozay.");
        goat.name = new Scanner(System.in).nextLine();

        System.out.println("Podaj gatunek kozy.");
        goat.species = new Scanner(System.in).nextLine();

        System.out.println("Podaj podgatunek: cat, goat, cow lub inny.");
        goat.podgatunek = new Scanner(System.in).nextLine();

        System.out.println("podaj wiek kozy");
        goat.age = new Scanner(System.in).nextInt();

        Animal cow = new Animal();
        System.out.println("Podaj imię krowy");
        cow.name = new Scanner(System.in).nextLine();

        System.out.println("Podaj gatunek krowy ");
        cow.species = new Scanner(System.in).nextLine();

        System.out.println("Podaj podgatunek: cat, goat, cow lub inny.");
        cow.podgatunek = new Scanner(System.in).nextLine();

        System.out.println("podaj wiek krowy");
        cow.age = new Scanner(System.in).nextInt();


        cat.wyswietlDaneZwierzecia();
        cat.dajGlos();
        goat.wyswietlDaneZwierzecia();
        goat.dajGlos();
        cow.wyswietlDaneZwierzecia();
        cow.dajGlos();
    }
}
