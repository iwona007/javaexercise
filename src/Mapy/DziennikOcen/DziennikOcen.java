package Mapy.DziennikOcen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DziennikOcen {

    Map<Student, List<Ocena>> dziennik;

    public DziennikOcen(){
        dziennik = new HashMap<>();
    }

    public void dodajOcene(Student student, Ocena ocena){
        List<Ocena> listaOcenStudenta;
        if (!dziennik.containsKey(student)){
            listaOcenStudenta = new ArrayList<>();
            listaOcenStudenta.add(ocena);
            dziennik.put(student, listaOcenStudenta);
        }else{
            listaOcenStudenta = dziennik.get(student);
            listaOcenStudenta.add(ocena);
        }
    }

    public void drukujOceny(){
        for(Map.Entry<Student, List<Ocena>> ocenyStudenta : dziennik.entrySet()){
            System.out.println( "Student:" + ocenyStudenta.getKey() + " Oceny: " + ocenyStudenta.getValue());
        }
    }





}
