package Mapy.DziennikOcen;

public class MainDziennik {
    public static void main(String[] args) {

        Student kasia = new Student("Kasia", "Kowalska");
        Student michal = new Student("Michał", "Nowak");
        Student alexy = new Student("Alexy", "Nowak");

        Przedmiot matma = new Przedmiot("Matematyka");
        Przedmiot hista = new Przedmiot("Historia");

        DziennikOcen dziennik = new DziennikOcen();
        dziennik.dodajOcene(kasia, new Ocena(5d, matma));
        dziennik.dodajOcene(michal, new Ocena(3.3d, hista));
        dziennik.dodajOcene(alexy, new Ocena(4d, matma));
        dziennik.dodajOcene(kasia, new Ocena(2.5d, hista));
        dziennik.dodajOcene(michal, new Ocena(4d, matma));
        dziennik.dodajOcene(alexy, new Ocena(4.5d, hista));

        dziennik.drukujOceny();
    }
}
