package Mapy.DziennikOcen;

public class Ocena {
    double ocena;
    Przedmiot przedmiot;


    public Ocena(double ocena, Przedmiot przedmiot){
        this.ocena = ocena;
        this.przedmiot = przedmiot;
    }

@Override
    public String toString(){
        return przedmiot + ":" + ocena;
}

}
